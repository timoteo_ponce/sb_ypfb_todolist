from fabric.api import *

#connect to server
env.hosts = ['20.20.100.44']
env.user = 'administrator'
env.password = 'admin'
#execute commands

def info():
	run( 'uname -ars' )

def touch():
	run( 'touch /opt/services/timoteo.txt' )

def copy():
	with settings( warn_only = True):
		run('mkdir /opt/services/timo')
	put('*.jar','/opt/services/timo',True)

def start():
	with cd( '/opt/services/timo' ):
		run('nohup java -jar SparkToDoList-1.0-SNAPSHOT-jar-with-dependencies.jar & sleep 5 ; exit 0')

def stop():
	with settings( warn_only=True):
		sudo("ps aux | grep Spark | awk '{print $2}' | xargs kill -9 $1")

def install():
	stop()
	copy()
	start()

