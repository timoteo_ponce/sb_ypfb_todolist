package ch.swissbytes.todo;

import org.junit.*;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class ToDoListIntegrationTest {

    static final ToDoList app = new ToDoList();

    @BeforeClass
    public static void setup() throws InterruptedException {
       app.startup(new EntityManager());
       Thread.sleep(700);//let's give it some time to start the server-socket, for slower machines
    }

    @AfterClass
    public static void tearDown(){
        app.shutdown();
    }

    @Test
    public void willListTodoItems() {
        HelperMethods.UrlResponse response;
        response = HelperMethods.doMethod("GET", "/items", null);

        assertThat(response, notNullValue());
        String body = response.body.trim();
        assertThat(body, notNullValue());
        assertThat(body.length() > 0, is(true));
        assertThat(200, is(response.status));
    }
}
