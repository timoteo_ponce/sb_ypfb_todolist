package ch.swissbytes.todo;

import ch.swissbytes.todo.model.ToDoItem;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class EntityManagerTest {
    private EntityManager entityManager;

    @Before
    public void setup() {
        entityManager = new EntityManager();
    }

    @Test
    public void getItemsReturnsEmptyListWhenNoItemsHaveBeenAdded() {
        assertThat(entityManager.getItems(), notNullValue());
        assertThat(entityManager.getItems().size(), is(0));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getItemsReturnsUnModifiableList() {
        entityManager.getItems().add(new ToDoItem(1, "title", "desc"));
    }

    @Test
    public void getCompletedItemsReturnsEmptyListWhenNoItemsHaveBeenAdded() {
        assertThat(entityManager.getCompletedItems(), notNullValue());
        assertThat(entityManager.getCompletedItems().size(), is(0));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getCompletedItemsReturnsUnModifiableList() {
        entityManager.getCompletedItems().add(new ToDoItem(1, "title", "desc"));
    }

    @Test
    public void addItemShouldAddItemToToDoItemList() {
        ToDoItem item = entityManager.addNewItem("title", "desc");
        assertThat(entityManager.getItems().size(), is(1));
        assertThat(entityManager.getItems().contains(item), is(true));
    }

    @Test
    public void getNextIdWillReturnAnIdBiggerThanTheLastId() {
        ToDoItem item = entityManager.addNewItem("title", "desc");
        assertThat(entityManager.getNextId(), greaterThan(item.getId()));
    }

    @Test
    public void getNextIdWIllReturnIdThatWillBeUsedWhenAnotherItemIsAdded() {
        int nextId = entityManager.getNextId();
        ToDoItem item = entityManager.addNewItem("title", "desc");
        assertThat(item.getId(), is(nextId));
    }

    @Test
    public void getToDoItemWillReturnItemReferencedById() {
        entityManager.addNewItem("title", "desc");
        ToDoItem item = entityManager.getToDoItem("1");
        assertThat(item.getId(), is(1));
    }

    @Test
    public void deleteToDoItemWillRemoveItem() {
        ToDoItem item = entityManager.addNewItem("title", "desc");
        assertThat(entityManager.getItems().size(), is(1));
        ToDoItem deletedItem = entityManager.deleteToDoItem("" + item.getId());
        assertThat(entityManager.getItems().size(), is(0));
        assertThat(deletedItem, notNullValue());
    }

    @Test
    public void completeToDoItemWillMoveItemToCompletedList() {
        ToDoItem item = entityManager.addNewItem("title", "desc");
        assertThat(entityManager.getItems().size(), is(1));
        assertThat(entityManager.getCompletedItems().size(), is(0));
        entityManager.completeToDoItem("" + item.getId());
        assertThat(entityManager.getItems().size(), is(0));
        assertThat(entityManager.getCompletedItems().size(), is(1));
    }
}
