package ch.swissbytes.todo;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

// selenium docs: http://docs.seleniumhq.org/docs/03_webdriver.jsp
public class ToDoListAcceptanceTestV2 {
    private static WebDriver driver;
    private static final ToDoList app = new ToDoList();

    @BeforeClass
    public static void setup() throws InterruptedException {
       app.startup(new EntityManager());
       Thread.sleep(700);//let's give it some time to start the server-socket, for slower machines

        driver = new FirefoxDriver();
    }

    @AfterClass
    public static void tearDown(){
        app.shutdown();
    }

    @Test
    public void testAddingAndCompletingItems() {
        String title = "My Item title";
        String description = "My item description";

        driver.get("http://localhost:4567/items");
        assertThatWeAreOnToDoListMainPage();

        addNewItemAndReturnToMainPage(title, description);
        viewAndCheckNewItemDetailsAndReturnToMainPage(title, description);
        completeItemAndCheckItIsNowInCompletedListByTitle(title);
    }

    private void completeItemAndCheckItIsNowInCompletedListByTitle(String title) {
        completeItemByTitle(title);
        assertThatItemIsNotInActiveToDoItems(title);
        assertThatItemIsInCompletedItems(title);
    }

    private void assertThatItemIsInCompletedItems(String title) {
        WebElement completedItem = findLastItemThatContainsText("#completed-items li", title);
        assertThat(completedItem, notNullValue());
    }

    private void assertThatItemIsNotInActiveToDoItems(String title) {
        WebElement newItem = findLastItemThatContainsText("#items li", title);
        assertThat(newItem, nullValue());
    }

    private void completeItemByTitle(String title) {
        WebElement item = findLastItemThatContainsText("#items li", title);
        WebElement completeLink = item.findElement(By.className("complete"));
        completeLink.click();
    }

    private void viewAndCheckNewItemDetailsAndReturnToMainPage(String title, String description) {
        findAndViewItemByTitle(title);
        assertThatBodyContainsText(title);
        assertThatBodyContainsText(description);
        returnToMainListPage();
        assertThatWeAreOnToDoListMainPage();
    }

    private void addNewItemAndReturnToMainPage(String title, String description) {
        findAndClickNewItemLink();
        assertThatWeAreOnNewItemPage();
        fillInTitleAndDescriptionAndSubmitForm(title, description);

        assertThatNewItemWasAddedSuccessfully();
        returnToMainListPage();
        assertThatWeAreOnToDoListMainPage();
    }

    private void findAndViewItemByTitle(String title) {
        WebElement item = findLastItemThatContainsText("#items li", title);
        WebElement viewLink = item.findElement(By.className("view"));

        viewLink.click();
    }

    private void returnToMainListPage() {
        WebElement backLink = driver.findElement(By.linkText("Back to list"));
        backLink.click();
    }

    private void assertThatNewItemWasAddedSuccessfully() {
        assertThatBodyContainsText("Item added successfully!!");
    }

    private void fillInTitleAndDescriptionAndSubmitForm(String title, String description) {
        WebElement titleField = driver.findElement(By.name("title"));
        WebElement descriptionField = driver.findElement(By.name("description"));
        titleField.sendKeys(title);
        descriptionField.sendKeys(description);
        titleField.submit();
    }

    private void assertThatWeAreOnNewItemPage() {
        assertThatBodyContainsText("Create new item");
    }

    private void findAndClickNewItemLink() {
        WebElement newItemLink = driver.findElement(By.linkText("New Item"));
        newItemLink.click();
    }

    private void assertThatWeAreOnToDoListMainPage() {
        assertThatBodyContainsText("My ToDo List");
    }

    private void assertThatBodyContainsText(String text) {
        assertThat(driver.findElement(By.cssSelector("body")).getText(), containsString(text));
    }

    private WebElement findLastItemThatContainsText(String selector, String text) {
        List<WebElement> items = driver.findElements(By.cssSelector(selector));
        WebElement foundItem = null;
        for (WebElement item : items) {
            if (item.getText().contains(text)) {
                foundItem = item;
            }
        }

        return foundItem;
    }
}
