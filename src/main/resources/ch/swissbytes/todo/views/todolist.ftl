<h1>My ToDo List</h1>
<ul id="items">
    <#list todoitems as item>
        <li id="item-${item.id}">
            ${item.id} - ${item.title} - <a href="/items/${item.id}/complete" class="complete">Complete</a> - <a href="/items/${item.id}" class="view">View</a> - <a href="/items/${item.id}/delete" class="delete">Delete</a>
        </li>
    </#list>
</ul>
<br />
<h2>Completed</h2>
<ul id="completed-items">
    <#list completeditems as item>
        <li id="completed-item-${item.id}">
            ${item.id} - ${item.title}
        </li>
    </#list>
</ul>
<br />
<br />
<a href="/items/new">New Item</a>