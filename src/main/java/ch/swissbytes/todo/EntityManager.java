package ch.swissbytes.todo;

import ch.swissbytes.todo.model.ToDoItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EntityManager {
    private List<ToDoItem> items;
    private List<ToDoItem> completedItems;

    public EntityManager() {
        items = new ArrayList<>();
        completedItems = new ArrayList<>();
    }

    public List<ToDoItem> getItems() {
        return Collections.unmodifiableList(items);
    }

    public List<ToDoItem> getCompletedItems() {
        return Collections.unmodifiableList(completedItems);
    }

    public int getNextId() {
        int id = 0;
        for (ToDoItem item : items) {
            if (id < item.getId()) {
                id = item.getId();
            }
        }

        return id + 1;
    }

    public ToDoItem addNewItem(String title, String description) {
        ToDoItem item = new ToDoItem(getNextId(), title, description);
        items.add(item);
        return item;
    }

    public ToDoItem getToDoItem(String id) {
        int item_id = Integer.parseInt(id);
        for (ToDoItem item : items) {
            if (item_id == item.getId()) return item;
        }
        return null;
    }

    public ToDoItem deleteToDoItem(String id) {
        int item_id = Integer.parseInt(id);
        for (ToDoItem item : items) {
            if (item_id == item.getId()) {
                items.remove(item);
                return item;
            }
        }
        return null;
    }

    public ToDoItem completeToDoItem(String id) {
        ToDoItem item = deleteToDoItem(id);
        if (item != null) {
            completedItems.add(item);
        }

        return item;
    }
}
