package ch.swissbytes.todo;

import ch.swissbytes.todo.model.ToDoItem;
import ch.swissbytes.todo.views.ToDoListFreeMarkerEngine;
import spark.ModelAndView;
import spark.TemplateEngine;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.SparkBase.stop;

public class ToDoList {
    public static void main(String[] args) {
        new ToDoList().startup(new EntityManager());
    }

    public void startup(EntityManager entityManager) {
        ToDoListFreeMarkerEngine templateEngine = new ToDoListFreeMarkerEngine();

        setupRoutes(entityManager, templateEngine);

        initialiseStartData(entityManager);
    }

    public void shutdown(){
        stop();
    }

    private void initialiseStartData(EntityManager entityManager) {
        entityManager.addNewItem("Item 1", "Description 1");
        entityManager.addNewItem("Item 2", "Description 2");
        ToDoItem item = entityManager.addNewItem("Item 3", "Description 3");
        entityManager.completeToDoItem("" + item.getId());
        item = entityManager.addNewItem("Item 4", "Description 4");
        entityManager.completeToDoItem("" + item.getId());
    }

    private void setupRoutes(EntityManager entityManager, ToDoListFreeMarkerEngine templateEngine) {
        setupItemsRoute(entityManager, templateEngine);
        setupNewItemRoute(templateEngine);
        setupCreateNewItemRoute(entityManager, templateEngine);
        setupViewItemRoute(entityManager, templateEngine);
        setupDeleteItemRoute(entityManager, templateEngine);
        setupCompleteItemRoute(entityManager, templateEngine);
        setup404ForAnyOtherRoutes(templateEngine);
    }

    private void setupDeleteItemRoute(EntityManager entityManager, ToDoListFreeMarkerEngine templateEngine) {
        get("/items/:id/delete", (request, response) -> {
            entityManager.deleteToDoItem(request.params(":id"));

            response.status(200);
            response.redirect("/items");
            return null;
        }, templateEngine);
    }

    private void setupCompleteItemRoute(EntityManager entityManager, ToDoListFreeMarkerEngine templateEngine) {
        get("/items/:id/complete", (request, response) -> {
            entityManager.completeToDoItem(request.params(":id"));

            response.status(200);
            response.redirect("/items");
            return null;
        }, templateEngine);
    }

    private void setupViewItemRoute(EntityManager entityManager, ToDoListFreeMarkerEngine templateEngine) {
        get("/items/:id", (request, response) -> {
            ToDoItem item = entityManager.getToDoItem(request.params(":id"));
            if (item != null) {
                Map<String, Object> attributes = new HashMap<>();
                attributes.put("title", item.getTitle());
                attributes.put("description", item.getDescription());


                return new ModelAndView(attributes, "viewitem.ftl");
            } else {
                response.status(404); // 404 Not found
                return new ModelAndView(null, "404.ftl");
            }
        }, templateEngine);
    }

    private void setupCreateNewItemRoute(EntityManager entityManager, TemplateEngine templateEngine) {
        post("/items/new", (request, response) -> {
            entityManager.addNewItem(request.queryParams("title"), request.queryParams("description"));

            Map<String, Object> attributes = new HashMap<>();
            attributes.put("success", true);
            response.status(201); // 201 Created

            return new ModelAndView(attributes, "newitem.ftl");
        }, templateEngine);
    }

    private void setupNewItemRoute(TemplateEngine templateEngine) {
        get("/items/new", (request, response) -> {
            Map<String, Object> attributes = new HashMap<>();

            return new ModelAndView(attributes, "newitem.ftl");
        }, templateEngine);
    }

    private void setupItemsRoute(EntityManager entityManager, TemplateEngine templateEngine) {
        get("/items", (request, response) -> {
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("todoitems", entityManager.getItems());
            attributes.put("completeditems", entityManager.getCompletedItems());

            return new ModelAndView(attributes, "todolist.ftl");
        }, templateEngine);
    }

    private void setup404ForAnyOtherRoutes(TemplateEngine templateEngine) {
        get("/*", (request, response) -> {
            response.status(404);

            return new ModelAndView(null, "404.ftl");
        }, templateEngine);
    }
}
